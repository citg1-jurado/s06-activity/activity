-- Jurado, Vladimir Ken P.
-- 1. Create an activity folder and a solution.sql file inside of it.

-- Create/add a database:
-- Syntax: CREATE DATABASE database_name;
CREATE DATABASE blog_db;
USE blog_db;

-- Create/Add a tables.
-- Syntax: CREATE TABLE table_name(column1, column2, PRIMARY KEY (id));
CREATE TABLE author(
    au_id VARCHAR(50) NOT NULL,
    au_lname VARCHAR(25) NOT NULL,
    au_fname VARCHAR(25) NOT NULL,
    address VARCHAR(50) NOT NULL,
    city VARCHAR(25) NOT NULL,
    state VARCHAR(10) NOT NULL,
    PRIMARY KEY(au_id)
);

CREATE TABLE publisher(
    pub_id INT NOT NULL,
    pub_name VARCHAR(50) NOT NULL,
    city VARCHAR(30) NOT NULL,
    PRIMARY KEY(pub_id)
);

CREATE TABLE title(
    title_id VARCHAR(50) NOT NULL,
    title VARCHAR(50) NOT NULL,
    type VARCHAR(25) NOT NULL,
    price FLOAT,
    pub_id INT NOT NULL,
    PRIMARY KEY(title_id),
    CONSTRAINT fk_title_publisher_id
		FOREIGN KEY (pub_id) REFERENCES publisher(pub_id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE author_title(
au_id VARCHAR(50) NOT NULL,
title_id VARCHAR(50) NOT NULL,
CONSTRAINT fk_author_title_author_id
	FOREIGN KEY (au_id) REFERENCES author(au_id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_author_title_title_id
	FOREIGN KEY (title_id) REFERENCES title(title_id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

-- Insert into author
INSERT INTO author (au_id, au_lname, au_fname, address, city, state)
VALUES('172-32-1176', 'White', 'Johnson', '10932 Bigge Rd.', 'Menlo Park', 'CA');

INSERT INTO author (au_id, au_lname, au_fname, address, city, state)
VALUES('213-46-8915', 'Green', 'Marjorie', '309 63rd St. #411', 'Oakland', 'CA');

INSERT INTO author (au_id, au_lname, au_fname, address, city, state)
VALUES('238-95-7766', 'Carson', 'Cheryl', '589 Darwin Ln.', 'Berkeley', 'CA');

INSERT INTO author (au_id, au_lname, au_fname, address, city, state)
VALUES('267-41-2394', "O'Leary", 'Michael', '22 Cleveland Av. #14', 'San Jose', 'CA');

INSERT INTO author (au_id, au_lname, au_fname, address, city, state)
VALUES('274-80-9391', 'Straight', 'Dean', '5420 College Av.', 'Oakland', 'CA');

INSERT INTO author (au_id, au_lname, au_fname, address, city, state)
VALUES('341-22-1782', 'Smith', 'Meander', '10 Mississippi Dr.', 'Lawrence', 'KS');

INSERT INTO author (au_id, au_lname, au_fname, address, city, state)
VALUES('409-56-7008', 'Bennet', 'Abraham', '6223 Bateman St.', 'Berkeley', 'CA');

INSERT INTO author (au_id, au_lname, au_fname, address, city, state)
VALUES('427-17-2319', 'Dull', 'Ann', '3410 Blonde St.', 'Palo Alto', 'CA');

INSERT INTO author (au_id, au_lname, au_fname, address, city, state)
VALUES('472-27-2349', 'Gringlesby', 'Burt', 'PO Box 792', 'Covelo', 'CA');

INSERT INTO author (au_id, au_lname, au_fname, address, city, state)
VALUES('486-29-1786', 'Locksley', 'Charlene', '18 Broadway Av.', 'San Francisco', 'CA');

-- Insert into publisher
INSERT INTO publisher (pub_id, pub_name, city)
VALUES(736, 'New Moon Books', 'Boston');

INSERT INTO publisher (pub_id, pub_name, city)
VALUES(877, 'Binnet & Hardley', 'Washington');

INSERT INTO publisher (pub_id, pub_name, city)
VALUES(1389, 'Algodata Infosystems', 'Berkeley');

INSERT INTO publisher (pub_id, pub_name, city)
VALUES(1622, 'Five Lakes Publishing', 'Chicago');

INSERT INTO publisher (pub_id, pub_name, city)
VALUES(1756, 'Ramona Publishers', 'Dallas');

INSERT INTO publisher (pub_id, pub_name, city)
VALUES(9901, 'GGG&G', 'Munchen');

INSERT INTO publisher (pub_id, pub_name, city)
VALUES(9952, 'Scootney Books', 'New York');

INSERT INTO publisher (pub_id, pub_name, city)
VALUES(9999, 'Lucerne Publishing', 'Paris');

-- Insert into title
INSERT INTO title (title_id, title, type, price, pub_id)
VALUES('BU1032', "The Busy Executive's Database Guide", 'business', '19.99', 1389);

INSERT INTO title (title_id, title, type, price, pub_id)
VALUES('BU1111', 'Cooking with Computers', 'business', '11.95', 1389);

INSERT INTO title (title_id, title, type, price, pub_id)
VALUES('BU2075','You Can Combat Computer Stress!', 'business', '2.99', 736);

INSERT INTO title (title_id, title, type, price, pub_id)
VALUES('BU7832', 'Straight talk About Computers', 'business', '19.99', 1389);

INSERT INTO title (title_id, title, type, price, pub_id)
VALUES('MC2222', 'Silicon Valley Gastronomic Treats', 'mod_cook', '19.99', 877);

INSERT INTO title (title_id, title, type, price, pub_id)
VALUES('MC3021', 'The Gourmet Microwave', 'mod_cook', '2.99', 877);

INSERT INTO title (title_id, title, type, price, pub_id)
VALUES('MC3026','The Psychology of Computer Cooking', 'UNDECIDED', '', 877);

INSERT INTO title (title_id, title, type, price, pub_id)
VALUES('PC1035','But is it User Friendly?', 'popular_comp', '22.95', 1389);

INSERT INTO title (title_id, title, type, price, pub_id)
VALUES('PC8888', 'Secrets of Silicon Valley', 'popular_comp', '20', 1389);

INSERT INTO title (title_id, title, type, price, pub_id)
VALUES('PC9999', 'Net Etiquette', 'popular_comp', '', 1389);

INSERT INTO title (title_id, title, type, price, pub_id)
VALUES('PS2091', 'Is Anger the Enemy?', 'psychology', '10.95', 736);

-- Insert into author_title
INSERT INTO author_title (au_id, title_id)
VALUES('172-32-1176', 'PS3333');

INSERT INTO author_title (au_id, title_id)
VALUES('213-46-8915', 'BU1032');

INSERT INTO author_title (au_id, title_id)
VALUES('213-46-8915', 'BU2075');

INSERT INTO author_title (au_id, title_id)
VALUES('238-95-7766', 'PC1035');

INSERT INTO author_title (au_id, title_id)
VALUES('267-41-2394', 'BU1111');

INSERT INTO author_title (au_id, title_id)
VALUES('267-41-2394', 'TC7777');

INSERT INTO author_title (au_id, title_id)
VALUES('274-80-9391', 'BU7832');

INSERT INTO author_title (au_id, title_id)
VALUES('409-56-7008', 'BU1032');

INSERT INTO author_title (au_id, title_id)
VALUES('427-17-2319', 'PC8888');

INSERT INTO author_title (au_id, title_id)
VALUES('472-27-2349', 'TC7777');

-- 2. Given the following data, provide the answer to the following:
-- a. List the books Authored by Marjorie Green.
SELECT author_title.au_id, author_title.title_id
FROM author_title
WHERE author_title.au_id = '213-46-8915';

-- b. List the books Authored by Michael O'Leary
SELECT author_title.au_id, author_title.title_id
FROM author_title
WHERE author_title.au_id = '267-41-2394';

-- c. Write the author/s of "The Busy Executives Database Guide".
SELECT author_title.au_id
FROM author_title
WHERE author_title.title_id = 'BU1032';

-- d. Identify the publisher of "But Is It User Friendly?"
SELECT publisher.pub_id, publisher.pub_name, title.title
FROM title, publisher
WHERE publisher.pub_id = '1389' AND title.title_id = 'PC1035';

-- e. List the books published by Algodata Infosystems.
SELECT title.pub_id, title.title
FROM title
WHERE title.pub_id = '1389';